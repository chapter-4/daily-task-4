module.exports = {
  // filter data based on fruit banana
  filterData: function (datas, attribute) { //deklarasi function filterData dengan parameter datas dan attribute
    let filtered_data = []; //deklarasi array kosong
    filtered_data = datas.filter((data) => { //looping data
      return data.favoriteFruit == attribute.favoriteFruit || data.age == attribute.age || data.eyeColor == attribute.eyeColor || data.company == attribute.company //return data yang sesuai dengan attribute
    })
    return filtered_data //return filtered_data
  },
}